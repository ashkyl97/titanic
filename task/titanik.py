import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    mr_list = []
    mrs_list = []
    miss_list = []
    def count_func(item):
        if 'Mr.' in item['Name']:
            mr_list.append(item['Age'])
        elif 'Mrs.' in item['Name']:
            mrs_list.append(item['Age'])
        elif 'Miss.' in item['Name']:
            miss_list.append(item['Age'])
    df.apply(count_func, axis=1)
    mr_series = pd.Series(mr_list)
    mrs_series = pd.Series(mrs_list)
    miss_series = pd.Series(miss_list)
    return [('Mr.', mr_series.isna().sum(), round(mr_series.median())), ('Mrs.', mrs_series.isna().sum(), round(mrs_series.median())), ('Miss.', miss_series.isna().sum(), round(miss_series.median()))]

